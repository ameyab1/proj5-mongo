Author: Ameya Bhandari
Contact: ameyab@uoregon.edu
Description: Brevet time calculator with Ajax and MongoDB

The brevet controls calculation rules are taken directly from the RUSA website and have an leeway of a few km past the defined brevet distances. The code for acp_times.py has been taken from the given acp_times_solved.py file that was given for this project. Unfortunately, while the times calculation work for the most part, the starting date and time are erroneous. 

On another note, I has to comment out the volumes in my docker-compose.yml because I was having the error that many other people have had in piazza, and using --build did not help. However, calc.html is pulled up when using docker-compose up, so at least that part of the project works.

